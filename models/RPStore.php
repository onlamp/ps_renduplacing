<?php
/**
 * This file is part of the Rendu placing for PrestaShop project.
 *
 * (c) Alexey Vasilyev <alex@onlamp.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once __DIR__ . '/RPObjectModel.php';

class RPStore extends RPObjectModel {

    /** @var int RPStore id */
    public $id_rp_store;
    /** @var  int user id */
    public $id_rp_user;
    /** @var  string store address */
    public $address;
    /** @var  string store address */
    public $address_json;
    /** @var  string store phone */
    public $phone;
    /** @var  string date created */
    public $date_add;
    /** @var  string date last updated */
    public $date_upd;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'rp_store',
        'primary' => 'id_rp_store',
        'fields' => array(
            'id_rp_store' => array(
                'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt',
                'required' => true, 'size' => 10),
            'id_rp_user' => array(
                'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt',
                'required' => true, 'size' => 10),
            'address' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString',
                'required' => true),
            'address_json' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString',
                'required' => true),
            'phone' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString',
                'required' => true, 'size' => 20),
            'date_add' => array(
                'type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' => array(
                'type' => self::TYPE_DATE, 'validate' => 'isDate')
        ),
    );

    /**
     * Make request for rendu
     *
     * @return string
     */
    public function getAPIRequest(){
        //var_dump($this->address_json);
        $jsonString = '{"address":'.$this->address_json.',';
        $jsonString .= '"phone":{"string":"'.$this->phone.'"}}';
        return $jsonString;
    }

    /**
     * Reload
     *
     * @return string
     */
    public function getApiUrl()
    {
        $base_api_url = parent::getApiUrl();
        return $base_api_url.'/users/'.$this->id_rp_user.'/stores';
    }

    /**
     * Parse responce to object
     *
     * @return bool
     */
    public function responseParse($resItem){
        try {
            $user = json_decode(Configuration::get('USER_'.RPDefines::$moduleName));
            $this->id_rp_user = $user->id;

            $this->id_rp_store = $resItem->id;

            $this->address = $this->uglyFix($resItem->address->formatted);
            $this->address_json = $this->uglyFix(json_encode($resItem->address));

            $phone = $resItem->phone;
            $this->phone = "+{$phone->region_code} ({$phone->code}) {$phone->number}";
        }
        catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Get Products by RPStore
     *
     * @param int $idLang
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     */
    public function getProducts()
    {
        return Db::getInstance()->executeS(
            'SELECT p.*'
            .' FROM `'._DB_PREFIX_.'rp_product` p'
            .' WHERE p.`id_rp_store` = '.(int) $this->id_rp_store
                .' AND p.`id_rp_user` = '.(int) $this->id_rp_user
        );
    }

    /**
     * Get Store id by RP user id
     *
     * @param int $userId
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     */
    public static function getUserStores($userId)
    {
        return Db::getInstance()->executeS(
            'SELECT s.id_rp_store'
            .' FROM `'._DB_PREFIX_.'rp_store` s'
            .' WHERE s.`id_rp_user` = '.(int) $userId
        );
    }
}