<?php
/**
 * This file is part of the Rendu placing for PrestaShop project.
 *
 * (c) Alexey Vasilyev <alex@onlamp.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once __DIR__ . '/RPObjectModel.php';

class RPCategory extends RPObjectModel {

    /** @var int RPCategory id */
    public $id_rp_category;
    /** @var int parent category id */
    public $parent_id_rp_category;
    /** @var  string name */
    public $name;
    /** @var  string date created */
    public $date_add;
    /** @var  string date last updated */
    public $date_upd;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'rp_category',
        'primary' => 'id_rp_category',
        'fields' => array(
            'id_rp_category' => array(
                'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt',
                'required' => true, 'size' => 10),
            'parent_id_rp_category' => array(
                'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt',
                'size' => 10),
            'name' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString',
                'size' => 150),
            'date_add' => array(
                'type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' => array(
                'type' => self::TYPE_DATE, 'validate' => 'isDate')
        ),
    );

    /**
     * Generate resource url
     * @return string
     */
    public function getApiUrl()
    {
        $base_api_url = parent::getApiUrl();
        return $base_api_url.'/categories';
    }

    /**
     * Parse responce to object
     * Not set object id
     *
     * @return bool
     */
    public function responseParse($resItem){
        try {

            $this->id_rp_category = $resItem->id;

            if ($resItem->parent_id){
                $this->parent_id_rp_category = $resItem->parent_id;
            }

            $this->name = $this->uglyFix($resItem->name);
        }
        catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Return categories
     *
     * @return array Categories
     */
    public static function getCategories()
    {
        $categories = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS(
		'SELECT t1.id_rp_category as id,'.
            'SUBSTRING('.
                'CONCAT_WS(" / ", t6.name, t5.name, t4.name, t3.name, t2.name, t1.name), '.
                '4, '.
                'LENGTH(CONCAT_WS(" / ", t6.name, t5.name, t4.name, t3.name, t2.name, t1.name)) '.
            ') as name '.
        'FROM '._DB_PREFIX_.'rp_category AS t1 '.
        'LEFT OUTER JOIN '._DB_PREFIX_.'rp_category AS t2 '.
            'ON t2.id_rp_category = t1.parent_id_rp_category '.
        'LEFT OUTER JOIN '._DB_PREFIX_.'rp_category AS t3 '.
            'ON t3.id_rp_category = t2.parent_id_rp_category '.
        'LEFT OUTER JOIN '._DB_PREFIX_.'rp_category AS t4 '.
            'ON t4.id_rp_category = t3.parent_id_rp_category '.
        'LEFT OUTER JOIN '._DB_PREFIX_.'rp_category AS t5 '.
            'ON t5.id_rp_category = t4.parent_id_rp_category '.
        'LEFT OUTER JOIN '._DB_PREFIX_.'rp_category AS t6 '.
            'ON t6.id_rp_category = t5.parent_id_rp_category '.
        'WHERE t1.parent_id_rp_category > 0'
        );

        return $categories;
    }
}