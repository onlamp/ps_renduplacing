<?php
/**
 * This file is part of the Rendu placing for PrestaShop project.
 *
 * (c) Alexey Vasilyev <alex@onlamp.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once __DIR__ . '/RPCurl.php';

class RPObjectModel extends ObjectModel {

    public static function createTable()
    {
        $definition = get_called_class()::$definition;
        $sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.$definition['table'].'` (';
        foreach ($definition['fields'] as $key => $field ) {
            $sql .= "`$key`";
            switch ($field['type']) {
                case self::TYPE_INT:
                    $sql .= ' int';
                    if (isset($field['size'])) {
                        $sql .= '('.$field['size'].')';
                    }
                    $sql .= ' unsigned NOT NULL';
                    break;
                case self::TYPE_STRING:
                    if (isset($field['size']) && $field['size'] <=255) {
                        $sql .= ' varchar('.$field['size'].')';
                    }
                    else {
                        $sql .= ' text';
                    }
                    $sql .= ' NOT NULL default \'\'';
                    break;
                case self::TYPE_DATE:
                    $sql .= ' datetime NULL';
                    break;
            }
            $sql .= ", ";
        }
        $sql = substr($sql, 0, -2);
        if (isset($definition['primary'])){
            $sql .=	', PRIMARY KEY (`'.$definition['primary'].'`)';
        }
		$sql .=	') ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';
        return Db::getInstance()->execute($sql);
    }

    public static function dropTable()
    {
        $definition = get_called_class()::$definition;
        $sql = 'DROP TABLE `'._DB_PREFIX_.$definition['table'].'`';
        return Db::getInstance()->execute($sql);
    }

    public function add($auto_date = true, $null_values = false)
    {
        if ($this->push()){
            return  parent::add($auto_date, $null_values);
        }
        else {
            return false;
        }
    }

    public function update($null_values = false)
    {
        if ($this->push()){
            return parent::update($null_values);
        }
        else {
            return false;
        }
    }

    /**
     * Saves current object to database (add or update)
     *
     * @param string $action
     *
     * @return bool Insertion result
     * @throws PrestaShopException
     */
    public function simpleSave($action = 'add')
    {
        if ($action=='add'){
            return parent::add();
        }
        else {
            return parent::update();
        }
    }

    public function delete()
    {
        $def = get_called_class()::$definition;
        $objId = $def['primary'];
        $curl = new RPCurl(Configuration::get('SID_'.RPDefines::$moduleName));

        $url = $this->getApiUrl().'/'.$this->$objId;
        $curl->delete($url);

        if ($curl->error) {
            throw new PrestaShopException($curl->getErrorMessage());
        }

        return parent::delete();
    }

    public function simpleDelete(){
        return parent::delete();
    }

    /**
     * Generate resource url
     * @return string
     */
    public function getApiUrl()
    {
        return RPDefines::getUrl();
    }

    /**
     * Push object to the rendu
     *
     * @return bool
     * @throws PrestaShopException
     */
    public function push()
    {
        $def = get_called_class()::$definition;
        $objId = $def['primary'];
        $curl = new RPCurl(Configuration::get('SID_'.RPDefines::$moduleName));

        $req = $this->getAPIRequest();

        if (!empty($this->$objId)){
            $url = $this->getApiUrl().'/'.$this->$objId;
            $curl->put($url, $req);
        }
        else {
            $curl->post($this->getApiUrl(), $req);
        }

        if ($curl->error) {
            $this->setSync(0,$curl->getErrorMessage(), true);
            throw new PrestaShopException($curl->getErrorMessage());
        }

        $this->responseParse($curl->response);
        $this->setSync(1);

        return true;
    }

    /**
     * Pull object form the rendu
     * @param array $params
     * @param int $records
     * @param int $page
     * @return bool
     * @throws PrestaShopException
     */
    public static function pull($params=array(), $records = 50, $page = 1, $upItems = array())
    {
        $def = get_called_class()::$definition;
        $objId = $def['primary'];
        $className = get_called_class();

        if (isset($params[$objId])){
            $obj = new $className($params[$objId]);
        }
        else {
            $obj = new $className;
        }

        if (count($params) > 0){
            foreach ($params as $k => $v) {
                $obj->$k = $v;
            }
        }

        try {
            $tmp = explode('/', $obj->getApiUrl());
            $resName = end($tmp);
        }
        catch (Exception $e) {
            throw new PrestaShopException($e->getMessage());
        }

        $curl = new RPCurl(Configuration::get('SID_'.RPDefines::$moduleName));

        if (isset($params[$objId])){
            $curl->get($obj->getApiUrl().'/'.$obj->$objId);
        }
        else {
            $curl->get($obj->getApiUrl(), array(
                'records' => $records,
                'page' => $page
            ));
        }

        if ($curl->error) {
            throw new PrestaShopException($curl->getErrorMessage());
        }

        $items = array();
        if (isset($curl->response->id)){
            $items[] = $curl->response;
        }
        elseif (isset($curl->response->$resName)) {
            $items = $curl->response->$resName;
        }

        if (count($items) == 0){
            return false;
        }

        foreach ($items as $item){
            $obj = new $className($item->id);
            $saveAction = ($obj->$objId != null) ? 'update' : 'add';

            if (!$obj->responseParse($item)){
                throw new PrestaShopException('Failed response parsing');
            }

            $obj->$objId = (int) $item->id;
            $obj->simpleSave($saveAction);
            $upItems[] = (int) $item->id;
        }

        if (isset($curl->response->recordsTotal)
            && $curl->response->recordsTotal > ($page*$records)){
            return $className::pull($params, $records, ++$page, $upItems);
        }

        $className::clear($upItems, $params);

        return true;

    }

    /** Set Syn
     *
     * @param int $isSync
     * @param string $comment
     * @return boolean
     */
    public function setSync($isSync, $comment = '', $save = false)
    {
        if (property_exists($this, 'is_sync')){
            $definition = get_called_class()::$definition;
            $idKey = $definition['primary'];
            $this->sync_comment = $comment;
            $this->is_sync = $isSync;
            $this->sync_date = date("Y-m-d H:i:s");

            if ($save) {
                $sql = "UPDATE `"._DB_PREFIX_.$definition['table']."`
                    SET `sync_date` = NOW(), `sync_comment` = '$comment', `is_sync` = $isSync
                    WHERE `{$definition['primary']}` = {$this->$idKey};";
                return Db::getInstance()->execute($sql);
            }
            return true;
        }
        return false;
    }

    /**
     * Make request for rendu
     *
     * @return array
     */
    public function getAPIRequest()
    {
        return array();
    }

    /**
     * Parse responce to object
     *
     * @return bool
     */
    public function responseParse($resItem)
    {
        return true;
    }

    /**
     * @param string $string
     * @return string string
     */
    public function uglyFix ($string)
    {
        $s = preg_replace('/\\\u0([0-9a-fA-F]{3})/','&#x\1;',$string);
        return html_entity_decode($s, ENT_NOQUOTES, 'UTF-8');
    }

    /**
     * Remove item without array
     * @param array $excludes
     * @return bool
     */
    public static function clear($excludes,$params)
    {
        $def = get_called_class()::$definition;

        $sql = new DbQuery();
        $sql->type('DELETE');
        $sql->from($def['table']);
        $where = '`'.$def['primary'].'` NOT IN ('.implode(',',$excludes).')';
        foreach ($params as $k=>$v){
            $where .= " AND `$k` = $v";
        }
        $sql->where($where);
        return Db::getInstance()->execute($sql);
    }
}