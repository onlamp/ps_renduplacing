<?php
/**
 * This file is part of the Rendu placing for PrestaShop project.
 * 
 * (c) Alexey Vasilyev <alex@onlamp.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class RPDefines {
    /* API urls defines */
    public static $server = 'http://192.168.56.1:4001';
    public static $apiVersion = '/app_dev.php/v1';
    public static function getUrl()
    {
        return RPDefines::$server.RPDefines::$apiVersion;
    }

    /* Module defines */
    public static $moduleName = 'RENDU_PLACING';

}