<?php
/**
 * This file is part of the Rendu placing for PrestaShop project.
 *
 * (c) Alexey Vasilyev <alex@onlamp.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require __DIR__ . '/../vendor/autoload.php';

class RPCurl extends \Curl\Curl
{
    public function __construct($sid = null, $ct = 'application/json;charset=UTF-8')
    {
        parent::__construct();
        if ($sid != null)
            $this->setCookieString($sid);
        $this->setHeader('Content-Type', $ct);
    }

    public function getErrorMessage ()
    {
        if (isset($this->response->error->exception[0]->message)){
            $message = $this->response->error->exception[0]->message;
        }
        elseif (isset($this->response[0]->message)) {
            $message = $this->response[0]->message;
        }
        else {
            $message = $this->response->error->message;
        }
        return $message;
    }
}