<?php
/**
 * This file is part of the Rendu placing for PrestaShop project.
 *
 * (c) Alexey Vasilyev <alex@onlamp.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once __DIR__ . '/RPObjectModel.php';
require __DIR__ . '/../vendor/autoload.php';

class RPProduct extends RPObjectModel {

    /** @var int RPProduct id */
    public $id_rp_product;

    /** @var int Product id */
    public $id_product;
    /** @var  int category id */
    public $id_rp_category;
    /** @var  int store id */
    public $id_rp_store;
    /** @var  int user id */
    public $id_rp_user;

    /** @var string Name */
    public $name;

    /** @var string description hash */
    public $hash_description;

    /** @var string image url from rendu */
    public $pic;
    /** @var string rendu images array in json */
    public $images;
    /** @var string price */
    public $price;
    /** @var string status */
    public $status;
    /** @var string custom content */
    public $custom_content;
    /** @var  string date created */
    public $date_add;
    /** @var  string date last updated */
    public $date_upd;
    /** @var  string date last sync */
    public $sync_date;
    /** @var  string sync comment*/
    public $sync_comment;
    /** @var  int sync status */
    public $is_sync;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'rp_product',
        'primary' => 'id_rp_product',
        'fields' => array(
            'id_rp_product' => array(
                'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt',
                'size' => 10),
            'id_product' => array(
                'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt',
                'size' => 10),
            'id_rp_category' => array(
                'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt',
                'required' => true, 'size' => 10),
            'id_rp_store' => array(
                'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt',
                'required' => true, 'size' => 10),
            'id_rp_user' => array(
                'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt',
                'required' => true, 'size' => 10),
            'name' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString',
                'size' => 150),
            'hash_description' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString',
                'size' => 150),
            'pic' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString',
                'size' => 150),
            'images' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString'),
            'price' => array(
                'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt',
                'required' => true, 'size' => 10),
            'status' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString',
                'size' => 150),
            'custom_content' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString'),
            'date_add' => array(
                'type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' => array(
                'type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'sync_date' => array(
                'type' => self::TYPE_DATE),
            'sync_comment' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString'),
            'is_sync' => array(
                'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt',
                'size' => 1)
        ),
    );

    /**
     * Make request for rendu
     *
     * @return string
     */
    public function getAPIRequest()
    {
        $product = new Product($this->id_product);
        $lang = Language::getLanguages()[0];

        $req = array(
            'name' => $product->name[$lang['id_lang']],
            'extraId' => $this->id_product,
            'content' => $this->makeContent(),
            'price' => $this->price,
            'category' => array('id' => $this->id_rp_category),
            'store' => array('id' => $this->id_rp_store),
        );

        if (!empty($this->status) && is_numeric($this->status)){
            $req['status'] = array('id' => $this->status);
        }

        if ($this->id_rp_product){
            $req['id'] = $this->id_rp_product;
        }
        return json_encode($req);
    }

    /**
     * Reload
     *
     * @return string
     */
    public function getApiUrl()
    {
        $base_api_url = parent::getApiUrl();
        return $base_api_url
            .'/users/'.$this->id_rp_user
            .'/stores/'.$this->id_rp_store
            .'/products';
    }

    /**
     * Parse responce to object
     *
     * @return bool
     */
    public function responseParse($resItem)
    {
        try {
            $this->id_rp_product = $resItem->id;

            $user = json_decode(Configuration::get('USER_'.RPDefines::$moduleName));
            $this->id_rp_user = $user->id;

            $this->id_rp_store = $resItem->store->id;

            $this->id_rp_category = $resItem->category->id;

            $this->name = $this->uglyFix($resItem->name);

            if (isset($resItem->images)){
                foreach (array_reverse($resItem->images) as $image) {
                    $this->pic = $image->path;
                    if ($image->main === true){
                        break;
                    }
                }
            }

            $images = array();
            if (!empty($this->images)) {
                $images = json_decode($this->images, true);
            }

            $resImages = json_decode(json_encode($resItem->images), true);
            // Чистим
            foreach($images as $i=>$v){
                $flag = true;
                foreach($resImages as $ii=>$vv){
                    if ($v['id'] == $vv['id']){
                        $flag = false;
                    }
                }
                if ($flag){
                    unset($images[$i]);
                }
            }
            $images = array_values($images);
            // Мержим
            foreach($resImages as $i=>$v){
                $flag = false;
                foreach($images as $ii=>$vv){
                    if ($v['id'] == $vv['id']){
                        $flag = true;
                    }
                }
                if (!$flag){
                    $images[] = $v;
                }
            }

            $this->images = json_encode($images);

            $this->price = $resItem->price;

            $this->status = $this->uglyFix($resItem->status->name);

        }
        catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Return array used products ids
     *
     * @return array Products id
     */
    public static function getExcludedProductsId()
    {
        $sql = new DbQuery();
        $sql->select('p.`id_product`');
        $sql->from('rp_product', 'p');
        $sql->where('p.`id_product` > 0 ');
        $result = Db::getInstance()->executeS($sql);
        return $result;
    }

    /**
     * Return array rp_products for pushing
     *
     * @return array Products id
     */
    public static function getProductsForPushing()
    {
        $sql = "SELECT `prp`.*
            FROM `"._DB_PREFIX_."rp_product` `prp`
            LEFT JOIN `"._DB_PREFIX_."product` `pp` USING (`id_product`)
            WHERE `pp`.`date_upd` > `prp`.`sync_date`";
        $result = Db::getInstance()->executeS($sql);
        return $result;
    }

    /**
     * Upload image
     * @return mixed
     */
    public function pushImages()
    {
        $lang = Language::getLanguages()[0];
        $product = new Product ($this->id_product);
        $images = $product->getImages($lang['id_lang']);
        $rImages = json_decode($this->images, true);

        $curl = new RPCurl(
            Configuration::get('SID_'.RPDefines::$moduleName));
        $url = $this->getApiUrl().'/'.$this->id_rp_product
            .'/images';

        // Сбор id для удаления
        $imagesForRemoving = array();
        foreach($rImages as $rImage){
            foreach($images as $image){
                if(isset($rImage['ps_id'])
                    && $image['id_image'] == $rImage['ps_id']){
                    continue 2;
                }
            }
            $imagesForRemoving[] = $rImage['id'];
        }

        foreach($imagesForRemoving as $id){
            $curl->delete($url."/".$id);
            if ($curl->error) {
                throw new Exception($curl->getErrorMessage());
            }
            foreach($rImages as $i => $rImage){
                if ($rImage['id'] == $id){
                    unset($rImages[$i]);
                }
            }
        }
        $rImages = array_values($rImages);

        //Добавление и обновление
        $pic = $this->pic;
        foreach($images as $image){
            foreach($rImages as $rImage){
                if($image['id_image'] == $rImage['ps_id']){
                    if($rImage['main'] != $image['cover']){
                        // Обновляем если
                        $rImage['main'] = ($image['cover'] == true);
                        $curl->put($url."/".$rImage['id'],$rImage);
                        if ($curl->error) {
                            throw new Exception($curl->getErrorMessage());
                        }
                        if ($image['cover'] == true) {
                            $pic = $curl->response->path;
                        }
                    }
                    continue 2;
                }
            }
            // Ддобавляем картинку
            $fileImage = new Image($image['id_image']);
            $filePath = _PS_ROOT_DIR_.'/img/p/'
                .$fileImage->getImgPath().'.'.$fileImage->image_format;

            if (!file_exists($filePath)){
                continue;
            }

            $curl->setHeader('Content-Type', 'multipart/form-data');
            $curl->post($url, array(
                'file' => new CURLFile($filePath),
                'main' => ($image['cover'] == true)
            ));

            if ($curl->error) {
                throw new Exception($curl->getErrorMessage());
            }

            $rImage = json_decode(json_encode($curl->response), true);
            $rImage['ps_id'] = $image['id_image'];

            if ($image['cover'] == true) {
                $pic = $curl->response->path;
            }
            $rImages[] = $rImage;
        }

        $this->pic = $pic;
        $this->images = json_encode($rImages);

        return true;
    }

    /**
     * Make content without html tags
     *
     * @return string
     */
    public function makeContent()
    {
        $product = new Product($this->id_product);
        $lang = Language::getLanguages()[0];

        $description_short = new \Html2Text\Html2Text(
            str_replace(PHP_EOL, '', $product->description_short[$lang['id_lang']]),
            array('width' => 0));

        $description = new \Html2Text\Html2Text(
            str_replace(PHP_EOL, '', $product->description[$lang['id_lang']]),
            array('width' => 0));

        $content = $description_short->getText()
                .$description->getText();
        if (!empty($this->custom_content)){
            $content .= $this->custom_content;
        }

        return $content;
    }

    public function makeHash ()
    {

        if (empty($this->id_product)){
            return;
        }

        $lang = Language::getLanguages()[0];
        $product = new Product ($this->id_product);
        $images = $product->getImages($lang['id_lang']);

        $str = $this->makeContent().$this->price.$this->custom_content;
        $str .= json_encode($images).$product->name[$lang['id_lang']];
        return md5($str);
    }
}