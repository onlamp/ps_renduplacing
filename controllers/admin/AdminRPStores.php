<?php
/**
 * This file is part of the Rendu placing for PrestaShop project.
 *
 * (c) Alexey Vasilyev <alex@onlamp.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

foreach (glob(__DIR__ .'/../../models/*.php') as $filename)
{
    require_once $filename;
}

class AdminRPStoresController extends ModuleAdminController
{
    public $bootstrap = true;

    public function __construct()
    {
        $this->table = 'rp_store';
        $this->className = 'RPStore';
        $this->lang = false;
        $this->deleted = false;
        $this->list_id = 'rp_store';
        $this->identifier = 'id_rp_store';
        $this->_defaultOrderBy = 'address';
        $this->_defaultOrderWay = 'ASC';
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );
        $this->context = Context::getContext();
        $this->fields_list = array(
            'id_rp_store' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'address' => array(
                'title' => $this->l('Address'),
                'width' => 'auto'
            ),
            'phone' => array(
                'title' => $this->l('Phone'),
                'align' => 'center'
            )
        );

        $this->rp_user = json_decode(Configuration::get('USER_'.RPDefines::$moduleName));

        parent::__construct();
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('tagify');
        $this->addJs('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places');
        $this->addJs(_MODULE_DIR_.$this->module->name.'/views/js/stores.js');
        $this->addJs(_MODULE_DIR_.$this->module->name.'/views/js/products.js');
        $this->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/renduplacing.css');

    }
    public function initPageHeaderToolbar()
    {
        switch ($this->display) {
            case '':
                $this->page_header_toolbar_btn['new_store'] = array(
                    'href' => self::$currentIndex.'&addrp_store&token='.$this->token,
                    'desc' => $this->l('Add new store', null, null, false),
                    'icon' => 'process-icon-new'
                );
                break;
            case 'view':
                $this->page_header_toolbar_btn['new_product'] = array(
                    'href' => self::$currentIndex
                        .'&add_rp_product&token='.$this->token
                        .'&viewrp_store&id_rp_store='.Tools::getValue('id_rp_store'),
                    'desc' => $this->l('Add new Product', null, null, false),
                    'icon' => 'process-icon-new'
                );
                break;
        }

        parent::initPageHeaderToolbar();
    }

    /**
     * AdminController::initToolbar() override
     * @see AdminController::initToolbar()
     *
     */
    public function initToolbar()
    {
        parent::initToolbar();
        switch ($this->display) {
            case '':
                $this->toolbar_btn['save-status'] = array(
                    'href' => self::$currentIndex.'&pull_rp_store&token='.$this->token,
                    'desc' => $this->l('Synchronize')
                );
                if ($this->can_import) {
                    $this->toolbar_btn['import'] = array(
                        'href' => $this->context->link->getAdminLink('AdminImport', true).'&import_type=manufacturers',
                        'desc' => $this->l('Import')
                    );
                }
                break;
            case 'view':
                $this->toolbar_btn['new'] = array(
                    'href' => self::$currentIndex
                        .'&add_rp_product&token='.$this->token
                        .'&viewrp_store&id_rp_store='.Tools::getValue('id_rp_store'),
                    'desc' => $this->l('Add product')
                );
                $this->toolbar_btn['save-status'] = array(
                    'href' => self::$currentIndex
                        .'&pull_rp_product&token='.$this->token
                        .'&viewrp_store&id_rp_store='.Tools::getValue('id_rp_store'),
                    'desc' => $this->l('Synchronize')
                );
                break;
        }
    }

    public function renderList()
    {
        $this->addRowAction('view');
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        $this->_where .= 'AND a.`id_rp_user` = '.$this->rp_user->id;

        $this->context->smarty->assign('title_list', $this->l('List of manufacturers'));

        if (!empty($this->_list)){
            echo '';
        }
        $this->content .= parent::renderList();
    }

    public function renderForm()
    {
        if (!($store = $this->loadObject(true))) {
            return;
        }

        if (empty($store->id_rp_store)){
            $store->id_rp_user = $this->rp_user->id;
            $store->address_json = 'f';
        }

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Store'),
                'icon' => 'icon-certificate'
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'id_rp_user',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Address'),
                    'name' => 'address',
                    'required' => true,
                    'hint' => $this->l('Invalid characters:').' &lt;&gt;;=#{}'
                ),
                array(
                    'type' => 'hidden',
                    'name' => 'address_json',
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Phone'),
                    'name' => 'phone',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->l('Invalid characters:').' &lt;&gt;;=#{}'
                ),
            )
        );
        if (!($manufacturer = $this->loadObject(true))) {
            return;
        }

        $this->fields_form['submit'] = array(
            'title' => $this->l('Save')
        );

        return parent::renderForm();
    }

    public function renderFormProduct()
    {
        // Change table and className for addresses
        $this->table = 'rp_product';
        $this->className = 'RPProduct';

        // Create Object Address
        $productIdParam = null;
        if (Tools::getValue('edit_rp_product')) {
            $productIdParam = Tools::getValue('edit_rp_product');
        }
        if (Tools::getValue('id_rp_product')) {
            $productIdParam = Tools::getValue('id_rp_product');
        }
        $product = new RPProduct($productIdParam);
        $res = $product->getFieldsRequiredDatabase();

        $required_fields = array();
        foreach ($res as $row) {
            $required_fields[(int)$row['id_required_field']] = $row['field_name'];
        }

        if (empty($product->id_rp_store)){
            $product->id_rp_user = $this->rp_user->id;
            $product->id_rp_store = Tools::getValue('id_rp_store');
        }

        $form = array(
            'legend' => array(
                'title' => $this->l('Product'),
                'icon' => 'icon-building'
            )
        );

        $form['input'] = array();

        if (!$product->id_product){
            $form['input'] = array(
                array(
                    'label' => $this->l('Search product'),
                    'type' => 'text',
                    'name' => 'name_product'
                ),
            );
        }

        $form['input'] = array_merge($form['input'], array(
            array(
                'type' => 'hidden',
                'name' => 'id_rp_product',
                'required' => true
            ),
            array(
                'type' => 'hidden',
                'name' => 'id_rp_user',
                'required' => true
            ),
            array(
                'type' => 'hidden',
                'name' => 'id_rp_store',
                'required' => true
            ),
            array(
                'type' => 'hidden',
                'name' => 'id_product',
                'required' => true
            ),
            array(
                'type' => 'hidden',
                'name' => 'pic',
                'required' => true
            ),
            array(
                'type' => 'select',
                'label' => $this->l('Category'),
                'name' => 'id_rp_category',
                'options' => array(
                    'query' => RPCategory::getCategories(),
                    'id' => 'id',
                    'name' => 'name'
                )
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Price, rub/day'),
                'name' => 'price',
                'col' => 6,
                'required' => true,
            ),
            array(
                'type' => 'textarea',
                'label' => $this->l('Terms'),
                'name' => 'custom_content',
                'required' => false,
                'rows' => 2,
                'cols' => 10,
                'col' => 6,
            ),
        ));

        $form['submit'] = array(
            'title' => $this->l('Save'),
        );

        $this->fields_form[0]['form'] = $form;
        $this->getlanguages();
        $helper = new HelperForm();
        $helper->show_cancel_button = true;

        $back = Tools::safeOutput(Tools::getValue('back', ''));
        if (empty($back)) {
            $back = self::$currentIndex.'&token='.$this->token;
        }
        if (!Validate::isCleanHtml($back)) {
            die(Tools::displayWarning());
        }

        $helper->back_url = $back;
        $helper->currentIndex = self::$currentIndex;
        $helper->token = $this->token;
        $helper->table = $this->table;
        $helper->identifier = $this->identifier;
        $helper->title = $this->l('Edit Addresses');
        $helper->id = $product->id;
        $helper->toolbar_scroll = true;
        $helper->languages = $this->_languages;
        $helper->default_form_language = $this->default_form_language;
        $helper->allow_employee_form_lang = $this->allow_employee_form_lang;
        $helper->fields_value = $this->getFieldsValue($product);
        $helper->toolbar_btn = $this->toolbar_btn;

        return $helper->generateForm($this->fields_form);
    }

    public function processSave()
    {
        try {
            //Добавление продукта
            $store= new RPStore(strval(Tools::getValue('id_rp_store')));
            $store->id_rp_user = $this->rp_user->id;
            $store->address = Tools::getValue('address');
            $store->address_json = Tools::getValue('address_json');
            $store->phone = Tools::getValue('phone');
            $store->push();
        }
        catch (Exception $e) {
            $this->displayWarning($this->l($e->getMessage()));
            $this->display = 'edit';
            return false;
        }

        return parent::processSave();
    }


    public function submitFormProduct()
    {
        try {
            //Добавление продукта
            $saveAction = 'add';
            if (Tools::getValue('id_rp_product')){
                $saveAction = 'upd';
            }
            $product = new RPProduct(strval(Tools::getValue('id_rp_product')));
            $product->id_rp_user = $this->rp_user->id;
            $product->id_rp_store = Tools::getValue('id_rp_store');
            $product->id_product = Tools::getValue('id_product');
            if (empty($product->id_product)){
                throw new Exception('The product is not selected');
            }
            $product->id_rp_category = Tools::getValue('id_rp_category');
            $product->price = Tools::getValue('price');
            $product->custom_content = Tools::getValue('custom_content');
            $product->hash_description = $product->makeHash();
            $product->validateFields();
            $product->push();
            $product->pushImages();
            $product->simpleSave($saveAction);
        }
        catch (Exception $e) {
            $this->displayWarning($this->l($e->getMessage()));
            return false;
        }
        return true;
    }

    public function initContent()
    {
        $this->initTabModuleList();
        // toolbar (save, cancel, new, ..)
        $this->initPageHeaderToolbar();
        $this->initToolbar();

        if (!$this->ajax){
            if (Tools::isSubmit('submitAddrp_product')){
                if ($this->submitFormProduct()){
                    $this->content .= $this
                        ->renderView(Tools::getValue('id_rp_store'));
                }
                else {
                    $this->content .= $this->renderFormProduct();
                }
            }
            elseif ($this->display == 'form_product') {
                $this->content .= $this->renderFormProduct();
            }
            elseif ($this->display == 'edit' || $this->display == 'add') {
                if (!$this->loadObject(true)) {
                    return;
                }
                $this->content .= $this->renderForm();
            } elseif ($this->display == 'view') {
                // Some controllers use the view action without an object
                if ($this->className) {
                    $this->loadObject(true);
                }
                if (Tools::isSubmit('pull_rp_product')) {
                    try {
                        RPProduct::pull(
                            array(
                                'id_rp_user' => $this->loadObject()
                                    ->id_rp_user,
                                'id_rp_store' => $this->loadObject()
                                    ->id_rp_store
                            )
                        );
                    }
                    catch (Exception $e){
                        $this->displayWarning($this->l($e->getMessage()));
                    }
                }
                if (Tools::isSubmit('remove_rp_product')) {
                    try {
                        $product = new RPProduct(Tools::getValue('remove_rp_product'));
                        $product->status = 300;
                        $product->save();
                        $product->simpleDelete();
                    }
                    catch (Exception $e){
                        $this->displayWarning($this->l($e->getMessage()));
                    }
                }
                $this->content .= $this->renderView();
            } else {
                if (Tools::isSubmit('pull_'.$this->table)) {
                    if (!RPStore::pull(array('id_rp_user' => $this->rp_user->id))){
                        $this->displayWarning($this->l('Error upload.'));
                    }
                }
                $this->content .= $this->renderList();
                //$this->content .= $this->renderOptions();
            }
        }

        $this->context->smarty->assign(array(
            'content' => $this->content,
            'url_post' => self::$currentIndex.'&token='.$this->token,
            'show_page_header_toolbar' => $this->show_page_header_toolbar,
            'page_header_toolbar_title' => $this->page_header_toolbar_title,
            'page_header_toolbar_btn' => $this->page_header_toolbar_btn
        ));
    }

    /**
     * Override to render the view page
     *
     * @return string
     */
    public function renderView($id = false)
    {
        if ($id) {
            $store = new RPStore($id);
        }
        elseif (!($store = $this->loadObject())) {
            return;
        }

        $products = $store->getProducts();

        //Set title
        $this->page_header_toolbar_title = $store->address;

        //Limit view products
        $records = 25;
        $page = 1;
        $totalProducts = count($products);
        $totalPages = intval($totalProducts / $records);
        if (($totalProducts % $records) > 0) {
            ++$totalPages;
        }
        if ((Tools::isSubmit('rp_product_page')) && Tools::getValue('rp_product_page')){
            $page = Tools::getValue('rp_product_page');
        }
        $products = array_slice($products, ($page-1)*$records, $records);

        $this->tpl_view_vars = array(
            'store' => $store,
            'products' => $products,
            'page' => $page,
            'totalPages' => $totalPages,
            'totalProducts' => $totalProducts,
            'stock_management' => Configuration::get('PS_STOCK_MANAGEMENT'),
            'shopContext' => Shop::getContext(),
            'apiServer' => RPDefines::$server,
            'currentViewUrl' => $this->context->link->getAdminLink('AdminRPStores')
                .'&viewrp_store'
                .'&id_rp_store='.$store->id_rp_store,
        );


        $helper = new HelperView($this);
        $this->setHelperDisplay($helper);
        $helper->tpl_vars = $this->getTemplateViewVars();
        $helper->base_folder = _PS_MODULE_DIR_.$this->module->name
            .'/views/templates/admin/rpstores/'.$helper->base_folder;
        $helper->title = $store->address;
        $view = $helper->generateView();

        return $view;
    }

    /**
     * AdminController::init() override
     * @see AdminController::init()
     */
    public function init()
    {
        parent::init();

        if (Tools::isSubmit('edit_rp_product')
        || Tools::isSubmit('add_rp_product')) {
            $this->display = 'form_product';
        }

        if (Tools::isSubmit('submitAddrp_product')){
            $this->display = 'view';
        }
    }

    public function ajaxProcessSearchProducts()
    {
        try {
            $products = Product::searchByName(
                (int)$this->context->language->id,
                pSQL(Tools::getValue('product_search')));

            $exludedProductsId = RPProduct::getExcludedProductsId();

            if (!$products){
                throw new Exception();
            }

            foreach($products as $k=>$product){
                foreach ($exludedProductsId as $exl){
                    if ($product['id_product'] == $exl['id_product']){
                        unset($products[$k]);
                    }
                }
            }
            if (count($products) == 0){
                throw new Exception();
            }
            $res = array(
                'found' => true,
                'products' => $products
            );
        }
        catch (Exception $e){
            $res = array('found' => false);
        }
        $this->content = json_encode($res);
    }

    public function ajaxProcessGetProduct()
    {
        $product = new Product (Tools::getValue('id_product'));
        $image = Product::getCover((int)Tools::getValue('id_product'));
        $product->description =
            $product->description[(int)$this->context->language->id];
        $product->description_short =
            $product->description_short[(int)$this->context->language->id];

        $link = new Link();
        $this->content = json_encode(array(
            'product' => $product,
            'image' => $link->getImageLink($product->link_rewrite, $image['id_image'], 'home_default')
        ));
    }
}