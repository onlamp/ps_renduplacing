{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}
  <div class="panel col-lg-12">
    <div class="panel-heading">
      {l s='Products'} <span class="badge">{$totalProducts}</span>
      <span class="panel-heading-action">
        {foreach from=$toolbar_btn item=btn key=k}
          {if $k != 'modules-list' && $k != 'back'}
            <a id="desc-{$table}-{if isset($btn.imgclass)}{$btn.imgclass}{else}{$k}{/if}"
                  class="list-toolbar-btn{if isset($btn.target) && $btn.target} _blank{/if}"
                  {if isset($btn.href)} href="{$btn.href|escape:'html':'UTF-8'}"{/if}
                  {if isset($btn.js) && $btn.js} onclick="{$btn.js}"{/if}>
              <span title="" data-toggle="tooltip" class="label-tooltip"
                  data-original-title="{l s=$btn.desc}" data-html="true" data-placement="top">
                <i class="process-icon-{if isset($btn.imgclass)}{$btn.imgclass}{else}{$k}{/if}{if isset($btn.class)} {$btn.class}{/if}"></i>
              </span>
            </a>
          {/if}
        {/foreach}
        <a class="list-toolbar-btn" href="javascript:location.reload();">
          <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Refresh list'}"
                data-html="true" data-placement="top">
            <i class="process-icon-refresh"></i>
          </span>
        </a>
      </span>
    </div>
    <div class="table-responsive-row clearfix">
      <table class="table rp_store">
        <tbody>
          {foreach $products AS $product}
            <tr>
              <td class="center">{$product['id_rp_product']}</td>
              <td>
                <img src="
                {if !empty($product['pic'])}
                    {$apiServer}{$product['pic']|replace:'.':'_tm.'}
                {else}
                    /modules/renduplacing/views/img/no-image.png
                {/if}
                " width="100">
              </td>
              <td>{$product['name']}</td>
              <td>{$product['price']} {l s='rub/day'}</td>
              <td>{l s=$product['status']}</td>
              <td class="text-right">
                <div class="btn-group-action"><div class="btn-group pull-right">
                  {if !empty($product['id_product'])}
                    <a href="{$currentViewUrl}&edit_rp_product={$product['id_rp_product']}"
                          class="btn btn-default" title="{l s='Edit'}">
                      <i class="icon-search-plus"></i> {l s='Edit'}
                    </a>
                  {/if}
                  <a href="{$currentViewUrl}&remove_rp_product={$product['id_rp_product']}"
                        title="{l s='Remove'}" class="delete btn btn-default">
                    <i class="icon-trash"></i> {l s='Remove'}
                  </a>
                </div></div>
              </td>
            </tr>
          {/foreach}
        </tbody>
      </table>
  </div>
    {if $totalPages>1}
      <div class="row">
        <div class="col-lg-6">
          <ul class="pagination pull-right">
            {for $i=1 to $totalPages}
              <li class="{($i == $page) ? 'active' : ''}">
                <a href="{$currentViewUrl}&rp_product_page={$i}"
                      class="pagination-link">{$i}</a>
              </li>
            {/for}
          </ul>
        </div>
      </div>
    {/if}
    <div class="panel-footer">
      <a id="desc-rp_product-back" class="btn btn-default"
            href="{$link->getAdminLink('AdminRPStores', true)}">
        <i class="process-icon-back "></i> <span>{l s='Back'}</span>
      </a>
    </div>
  </div>
{/block}