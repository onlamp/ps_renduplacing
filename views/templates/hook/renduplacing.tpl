<div class="panel">
    <h3>
        {l s='Profile' mod='renduplacing'}
        <span class="panel-heading-action">
            <a id="desc-configuration-new" class="list-toolbar-btn" href="{$logout}">
                <span title="" data-toggle="tooltip" class="label-tooltip"
                      data-original-title="{l s='Logout' mod='renduplacing'}" data-html="true">
                    <i class="process-icon-delete"></i>
                </span>
            </a>
        </span>
    </h3>
    <div id="admin_renduplacing" class="row">
        <!-- uiView:  -->
        <div class="col-md-12 main">
          <div class="row ng-scope">
            <div id="userpic_form" class="col-md-3 col-sm-4 col-md-offset-2 ng-scope">
                <div class="thumbnail">
                    <div class="text-center">
                        {if isset($rp_user->pic->path)}
                            <img alt="Admin" title="Admin"
                                 src="{$rp_domen}{$rp_user->pic->path|replace:'.':'_m.'}">
                        {else}
                            <i class="icon-user" aria-hidden="true"></i>
                        {/if}
                    </div>
                    <div class="caption text-center">
                        <p>{$rp_user->username}</p>
                        <p>{$rp_user->email}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-8 ng-scope" ng-controller="UserCtrl">
                <ul class="list-unstyled">
                    {if isset($rp_chips->st)}
                        <li><h2>{l s='Stores' mod='renduplacing'}: {$rp_chips->st}</h2></li>
                    {/if}
                    {if isset($rp_chips->pr)}
                        <li><h2>{l s='Products' mod='renduplacing'}: {$rp_chips->pr}</h2></li>
                    {/if}
                    {if isset($rp_chips->pr)}
                        <li><h2>{l s='Batch upload' mod='renduplacing'}</h2></li>
                    {/if}
                </ul>
            </div>
          </div>
          <div class="row ng-scope">
            <div class="col-sm-12">
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel">
                  <div role="tab" id="headingOne">
                    <h4 class="panel-title">
                      <a role="button" data-toggle="collapse" data-parent="#accordion"
                         href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        {l s='Enable auto synchronizing' mod='renduplacing'}
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                      <p>{l s='To enable automatic synchronization, you need to give the address to your task manager' mod='renduplacing'}</p>
                      <p><code>{$cron_url}</code></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>