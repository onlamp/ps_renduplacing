/**
 * This file is part of the Rendu placing for PrestaShop project.
 *
 * (c) Alexey Vasilyev <alex@onlamp.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

var updateUrl = function(){
    url = window.location.toString();
    url = url.replace('&pull_rp_store','');
    url = url.replace('&pull_rp_product','');
    history.replaceState(
        '',
        document.title,
        url
    );
};
updateUrl();

google.maps.event.addDomListener(window, 'load', function () {
  var separator = ', ';
  var mask = ['administrative_area_level_1', 'locality', 'route', 'street_number'];
  var places = new google.maps.places.Autocomplete(document.getElementById('address'));
  google.maps.event.addListener(places, 'place_changed', function () {
    var place = places.getPlace();
    address = {
      "formatted":place.formatted_address,
      "lat":place.geometry.location.lat().toFixed(7),
      "lng":place.geometry.location.lng().toFixed(7)
    }

    var adr_cpn = place.address_components;
    for (var i = 0; i < adr_cpn.length; i++){
      type = adr_cpn[i]['types'][0];
      name = adr_cpn[i]['short_name'];
      address[type] = name;
    }

    var formatted = '';
    for (var i = 0; i < mask.length; i++){
      for (var j = 0; j < adr_cpn.length; j++){
        if (adr_cpn[j].long_name.indexOf('город')>=0
          && mask[i]=="administrative_area_level_1" ){
          break;
        }

        f = false;
        for (var k = 0; k < adr_cpn[j].types.length; k++){
          if (adr_cpn[j].types[k] == mask[i]){
            f = true;
          }
        }

        if (f || (mask[i] == '' && adr_cpn[j].types.length==0)){
          formatted = formatted + adr_cpn[j].long_name + separator;
        }
      }
    }
    formatted = formatted.substring(0, formatted.length - 2);
    address.formatted = formatted;

    document.getElementById('address_json').value = JSON.stringify(address)
    document.getElementById('address').value = formatted
  });
});