/**
 * This file is part of the Rendu placing for PrestaShop project.
 *
 * (c) Alexey Vasilyev <alex@onlamp.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

var render_typeahead = function(element, select, data){
  var t_class = 'rp_tahead', conteiner;
  if (element.parent().find('.'+t_class).length == 0) {
    conteiner = $('<div class="'+t_class+' list-group hidden"></div>');
    element.parent().append(conteiner);
  }
  else {
    conteiner = element.parent().find('.'+t_class).eq(0);
  }

  conteiner.html('');

  $.each(data, function(i, v){
    var a = $('<a class="list-group-item" href=#></a>');
    a.html(v.name);
    a.data('idp', v.id_product);
    conteiner.append(a);
  })

  conteiner.on('click','.list-group-item',function(evt){
    evt.preventDefault();
    select($(this));
    element.val($(this).html());
    $(this).parent().addClass('hidden');
  });

  conteiner.removeClass('hidden');
}

var render_product = function(element, id_product){
  if(id_product == ''){
    return;
  }
  var url = window.location.toString();
  url = url+'&ajax=true&action=getProduct';
  url = url+'&id_product='+id_product;

  var make_view = function(product, image){
    if (element.parent().find('.product_view').length == 0) {
      rows = $('<div class="product_view row">');
      element.parent().append(rows);
    }
    else {
      rows = element.parent().find('.product_view').eq(0);
    }
    rows.html('');

    row_left = $('<div class="col-xs-2">');
    img = $('<img class="img-responsive">');
    img.attr('src','http://'+image);
    img.attr('alt',product.name[1]);
    row_left.append(img);
    rows.append(row_left);

    row_body = $('<div class="col-xs-10">');
    heading = $('<h4>');
    heading.html(product.name[1]);
    row_body.append(heading);
    row_body.append(product.description_short);
    row_body.append(product.description);
    rows.append(row_body);

    element.parent().removeClass('hide');
  }

  $.getJSON( url, function( data ) {
    make_view(data.product, data.image);
  });
}

var add_search_listener = function(){
  $('#rp_product_form').on('keyup', '#name_product', function(){
    var than = $(this);
    var select = function(item){
      var id = item.data('idp');
      var product = $('#id_product')
      product.val(id);
      product.change();
    }

    if (than.val().length > 2){
      var url = window.location.toString();
      url = url+'&ajax=true&action=searchProducts';
      url = url+'&product_search='+$(this).val();

      $.getJSON( url, function( data ) {
        if (data.found == true){
          render_typeahead(than, select, data.products);
        }
      });
    }
    else {
      render_typeahead(than, select, []);
    }
  })
}

var add_product_listener = function(){
  $('#rp_product_form').on('change', '#id_product', function(){
    var than = $(this);
    render_product(than, than.val());
  })
}

$('#name_product').attr('autocomplete','off');

$(document).ready(function(){
  if ($('#id_product').val()){
    render_product($('#id_product'), $('#id_product').val());
  }

  add_search_listener();
  add_product_listener();
});
