<?php
/**
 * This file is part of the Rendu placing for PrestaShop project.
 * 
 * (c) Alexey Vasilyev <alex@onlamp.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
foreach (glob(__DIR__ .'/models/*.php') as $filename)
{
    require_once $filename;
}

if (isset($_GET['secure_key'])){
    $secureKey = md5(_COOKIE_KEY_.Configuration::get('PS_SHOP_NAME'));
    if (!empty($secureKey) AND $secureKey === $_GET['secure_key']){
        // Получаем магазины и товары с ренду
        $user = json_decode(Configuration::get('USER_'.RPDefines::$moduleName));
        RPStore::pull(array('id_rp_user' => $user->id));
        $stores = RPStore::getUserStores($user->id);
        if (count($stores) > 0){
            foreach ($stores as $store){
                RPProduct::pull(
                    array(
                        'id_rp_user' => $user->id,
                        'id_rp_store' => $store['id_rp_store']
                    )
                );
            }
        }
        // получаем список измененных товаров с даты последней отправки и отправляем на ренду
        $products = RPProduct::getProductsForPushing();
        if (count($products) == 0 ){
            return;
        }
        foreach($products as $productItem){
            $product = new RPProduct($productItem['id_rp_product']);
            $product->push();
            $product->pushImages();
            $product->simpleSave('upd');
        }
    }
}
