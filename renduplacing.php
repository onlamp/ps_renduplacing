<?php
/**
 * This file is part of the Rendu placing for PrestaShop project.
 *
 * (c) Alexey Vasilyev <alex@onlamp.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

if (!defined('_PS_VERSION_'))
    exit;

foreach (glob(__DIR__ .'/models/*.php') as $filename)
{
    require_once $filename;
}

class RenduPlacing extends Module
{
    public function __construct()
    {
        $this->name = 'renduplacing';
        $this->tab = 'market_place';
        $this->version = '0.1.0';
        $this->author = 'onLamp team';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Rendu placing');
        $this->description = $this->l('Description of my module.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (function_exists('curl_init') == false) {
            $this->warning .= $this->l('To be able to use this module, please activate cURL (PHP extension).');
        }

        $this->url = RPDefines::getUrl();

        //if (!Configuration::get(R_MN))
        //    $this->warning = $this->l('No name provided');
    }

    public function install()
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install()
            || !Configuration::updateValue('USER_'.RPDefines::$moduleName, '')
            || !Configuration::updateValue('CHIPS_'.RPDefines::$moduleName, '')
            || !Configuration::updateValue('SID_'.RPDefines::$moduleName, '')
            || !RPStore::createTable()
            || !RPProduct::createTable()
            || !RPCategory::createTable()
            || !RPCategory::pull()

        )
            return false;

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()
            || !$this->_unInstallTabs()
            || !RPStore::dropTable()
            || !RPProduct::dropTable()
            || !RPCategory::dropTable()
            || !Configuration::deleteByName('USER_'.RPDefines::$moduleName)
            || !Configuration::deleteByName('CHIPS_'.RPDefines::$moduleName)
            || !Configuration::deleteByName('SID_'.RPDefines::$moduleName)
        )
            return false;

        return true;
    }

    private function _installTabs() {
        foreach ($this->getTabsArr() as $tab) {
            $this->createTab(
                $tab['class_name'],
                $tab['names'],
                $tab['parent'] ? Tab::getIdFromClassName($tab['parent']) : 0);
        }
        return true;
    }

    private function getTabsArr ()
    {
        return array(
            array(
                'class_name' => 'AdminRPStores',
                'parent' => 'AdminCatalog',
                'names' => array(
                    'en' => 'Rendu placing'
                )
            )
        );
    }

    private function createTab ($className, $names, $idParent=0)
    {
        if (!Tab::getIdFromClassName($className)) {
            $tab = new Tab();
            $tab->class_name = $className;
            $tab->module = $this->name;
            $tab->id_parent = $idParent;
            $tab->active = 1;
            $tab->name = array();
            foreach (Language::getLanguages(false) as $lang) {
                if (isset($names[$lang['iso_code']])) {
                    $tab->name[(int)$lang['id_lang']] = $names[$lang['iso_code']];
                } else {
                    $tab->name[(int)$lang['id_lang']] = $names['en'];
                }
            }
            if (!$tab->save()) {
                return $this->_abortInstall(
                    $this->l("Unable to create the #{$names['en']} tab"));
            }
        }
    }

    // Uninstall Tabs on Module uninstall
    private function _unInstallTabs()
    {
        // Delete the Module Back-office tab
        foreach ($this->getTabsArr() as $tab) {
            if ($id_tab = Tab::getIdFromClassName($tab['class_name'])) {
                $tab = new Tab((int)$id_tab);
                $tab->delete();
            }
        }
        return true;
    }

    public function initToolbar()
    {
        $current_index = AdminController::$currentIndex;
        $token = Tools::getAdminTokenLite('AdminModules');
        $back = Tools::safeOutput(Tools::getValue('back', ''));
        if (!isset($back) || empty($back))
            $back = $current_index.'&amp;configure='.$this->name.'&token='.$token;
        switch ($this->_display)
        {
            case 'add':
                $this->toolbar_btn['cancel'] = array(
                    'href' => $back,
                    'desc' => $this->l('Cancel')
                );
                break;
            case 'edit':
                $this->toolbar_btn['cancel'] = array(
                    'href' => $back,
                    'desc' => $this->l('Cancel')
                );
                break;
            case 'index':
                $this->toolbar_btn['new'] = array(
                    'href' => $current_index.'&amp;configure='.$this->name.'&amp;token='.$token.'&amp;addBlockCMS',
                    'desc' => $this->l('Add new')
                );
                break;
            case 'login':
                $this->toolbar_btn['sign_up'] = array(
                    'href' => $current_index.'&amp;configure='.$this->name.'&amp;token='.$token.'&amp;signUp'
                        .$this->name,
                    'desc' => $this->l('Sign up')
                );

                break;
            default:
                break;
        }
        return $this->toolbar_btn;
    }

    public function getContent()
    {
        // router
        $this->display = 'LoginForm';
        if (Tools::isSubmit('signUp'.get_class($this)) ) {
            $this->display = 'SignUpForm';
        }
        /*
        if (Tools::isSubmit('login'.get_class($this))) {
            $this->display = 'LoginForm';
        }
        */
        if (!empty(Configuration::get('USER_'.RPDefines::$moduleName))) {
            $this->display = 'IndexView';
        }
        $output = '';

        try {
            if (Tools::isSubmit('logout'.get_class($this))) {
                $this->submitLogoutForm();
            }
            elseif (Tools::isSubmit('submitSignUp'.get_class($this))){
                $this->display = 'SignUpForm';
                $this->submitSignUpForm();
            }
            elseif (Tools::isSubmit('submitLogin'.get_class($this))) {
                $this->display = 'LoginForm';
                $this->submitLoginForm();
            }
        }
        catch (Exception $e){
            var_dump($this->display);
            $output .= $this->displayError($this->l($e->getMessage()));
        }

        $viewName = "display{$this->display}";
        //var_dump($viewName);
        $output .= $this->$viewName();
        return $output;
    }

    private function displayIndexView()
    {
        $current_index = AdminController::$currentIndex;
        $token = Tools::getAdminTokenLite('AdminModules');
        $cronJobUrl = 'http://'.ShopUrl::getMainShopDomain($this->context->shop->id)
            .'/modules/renduplacing'
            .'/cron_rp_push.php?secure_key='
            .md5(_COOKIE_KEY_.Configuration::get('PS_SHOP_NAME'));
        $this->smarty->assign(
            array(
                'rp_user' => json_decode(Configuration::get('USER_'.RPDefines::$moduleName)),
                'rp_chips' => json_decode(Configuration::get('CHIPS_'.RPDefines::$moduleName)),
                'cron_url' => $cronJobUrl,
                'rp_domen' => Configuration::get('URI_'.RPDefines::$moduleName),
                'logout' => $current_index.'&amp;configure='.$this->name.'&amp;token='.$token.'&amp;logout'.get_class($this)
            )
        );

        Tools::addCSS($this->_path.'css/renduplacing.css', 'all');
        return $this->display(dirname(__FILE__), 'renduplacing.tpl');
    }

    public function displayLoginForm()
    {
        $current_index = AdminController::$currentIndex;
        $token = Tools::getAdminTokenLite('AdminModules');

        $this->_display = 'login';

        // Init Fields form array
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Login on Rendu'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'class' => '',
                    'label' => $this->l('Email'),
                    'name' => 'email',
                    'required' => true
                ),
                array(
                    'type' => 'password',
                    'label' => $this->l('Password'),
                    'name' => 'password',
                    'required' => true
                ),
                array(
                    'type' => 'hidden',
                    'name' => 'client',
                    'required' => true
                )
            ),
            'submit' => array(
                'title' => $this->l('Login'),
                'name' => 'submitLogin'.get_class($this),
                'icon' => '',
                'class' => 'btn btn-success pull-right btn-lg'
            ),
            'buttons' => array(
                array(
                    'href' => $current_index.'&amp;configure='.$this->name.'&amp;token='.$token.'&amp;signUp'.get_class($this),
                    'title' => $this->l('Sign up'),
                    'icon' => '',
                    'class' => 'btn btn-info btn-lg'
                )
            )
        );

        $helper = $this->initForm();

          // Load current value
        $helper->fields_value['password'] = '';
        $helper
            ->fields_value['email'] =
            Tools::isSubmit('email') ? strval(Tools::getValue('email')) : Configuration::get('PS_SHOP_EMAIL');
        $helper->fields_value['client'] = 'PrestaShop '._PS_VERSION_.' '.Tools::getHttpHost(false);

        return $helper->generateForm($fields_form);
    }

    public function displaySignUpForm()
    {
        $current_index = AdminController::$currentIndex;
        $token = Tools::getAdminTokenLite('AdminModules');

        $this->_display = 'sign_up';

        // Init Fields form array
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Sign up on Rendu'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'class' => '',
                    'label' => $this->l('Name'),
                    'name' => 'name',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'class' => '',
                    'label' => $this->l('Email'),
                    'name' => 'email',
                    'required' => true
                ),
                array(
                    'type' => 'hidden',
                    'name' => 'client',
                    'required' => true
                )
            ),
            'submit' => array(
                'title' => $this->l('Sign up'),
                'class' => 'btn btn-default pull-right',
                'name' => 'submitSignUp'.get_class($this),
                'icon' => '',
                'class' => 'btn btn-success pull-right btn-lg'
            ),
            'buttons' => array(
                array(
                    'href' => $current_index.'&amp;configure='.$this->name.'&amp;token='.$token,
                    'title' => $this->l('Login'),
                    'icon' => '',
                    'class' => 'btn btn-info btn-lg'
                )
            )
        );

        $helper = $this->initForm();

        // Load current value
        $helper
            ->fields_value['name'] =
            Tools::isSubmit('name') ? strval(Tools::getValue('name')) : Configuration::get('PS_SHOP_NAME');
        $helper
            ->fields_value['email'] =
            Tools::isSubmit('email') ? strval(Tools::getValue('email')) : Configuration::get('PS_SHOP_EMAIL');
        $helper->fields_value['client'] = 'PrestaShop '._PS_VERSION_.' '.Tools::getHttpHost(false);

        return $helper->generateForm($fields_form);
    }

    protected function initForm()
    {
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;

        // Module, token and currentIndex
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->languages = $this->context->controller->_languages;
        $helper->default_form_language = $this->context->controller->default_form_language;
        $helper->allow_employee_form_lang = $this->context->controller->allow_employee_form_lang;

        // Title and toolbar
        $helper->toolbar_scroll = true;
        $helper->show_toolbar = true;

        return $helper;
    }

    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path.'css/renduplacing.css', 'all');
    }

    private function submitSignUpForm()
    {
        $curl = new RPCurl();
        $curl->post($this->url.'/singup', array(
            'user' => array(
                'username' => strval(Tools::getValue('name')),
                'email' => strval(Tools::getValue('email')),
                'client' => strval(Tools::getValue('client'))
            ),
            'company' => array(
                'type' => 1,
                'name' => ""
            )
        ));

        if ($curl->error) {
            throw new Exception($curl->getErrorMessage());
        }

        $this->postLogin($curl);
    }

    private function submitLoginForm()
    {
        $curl = new RPCurl();
        $curl->post($this->url.'/login', array(
            'password' => strval(Tools::getValue('password')),
            'email' => strval(Tools::getValue('email')),
            'client' => strval(Tools::getValue('client'))
        ));

        if ($curl->error) {
            throw new Exception($curl->getErrorMessage());
        }

        $this->postLogin($curl);
    }

    private function submitLogoutForm()
    {
        $curl = new RPCurl(Configuration::get('SID_'.RPDefines::$moduleName));
        $curl->get($this->url.'/logout');
        if ($curl->error) {
            throw new Exception($curl->getErrorMessage());
        }
        $this->postLogout();
    }

    private function postLogin($curl)
    {
        Configuration::updateValue('USER_'.RPDefines::$moduleName
            , json_encode($curl->response->user));
        Configuration::updateValue('CHIPS_'.RPDefines::$moduleName
            , json_encode($curl->response->chips));

        $sid = 'sid='.$curl->getResponseCookie('sid');
        $sid .= '; domain='.parse_url($this->url)['host'];
        Configuration::updateValue('SID_'.RPDefines::$moduleName, $sid);

        RPStore::pull(array('id_rp_user' => $curl->response->user->id));
        $stores = RPStore::getUserStores($curl->response->user->id);
        if (count($stores) > 0){
            foreach ($stores as $store){
                RPProduct::pull(
                    array(
                        'id_rp_user' => $curl->response->user->id,
                        'id_rp_store' => $store['id_rp_store']
                    )
                );
            }
        }

        // pull Products
        $this->_installTabs();

        $this->display = 'IndexView';
    }

    private function postLogout()
    {
        Configuration::updateValue('USER_'.RPDefines::$moduleName, '');
        Configuration::updateValue('CHIPS_'.RPDefines::$moduleName, '');
        Configuration::updateValue('SID_'.RPDefines::$moduleName, '');
        $this->_unInstallTabs();
        $this->display = 'LoginForm';
    }
}