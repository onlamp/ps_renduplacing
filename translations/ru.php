<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{renduplacing}prestashop>renduplacing_c888438d14855d7d96a2724ee9c306bd'] = 'Настройки изменены';
$_MODULE['<{renduplacing}prestashop>renduplacing_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{renduplacing}prestashop>renduplacing_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{renduplacing}prestashop>renduplacing_630f6dc397fe74e52d5189e2c80f282b'] = 'Назад к списку';
$_MODULE['<{renduplacing}prestashop>renduplacing_9a843f20677a52ca79af903123147af0'] = 'Добро пожаловать!';
$_MODULE['<{renduplacing}prestashop>renduplacing_bca9994ba4f05bf2a16b27e9aa3d2840'] = 'Привет, %1$s!';
$_MODULE['<{renduplacing}prestashop>renduplacing_c66b10fbf9cb6526d0f7d7a602a09b75'] = 'Кликни по этой ссылке';
$_MODULE['<{renduplacing}prestashop>renduplacing_f42c5e677c97b2167e7e6b1e0028ec6d'] = 'Нажми меня!';
